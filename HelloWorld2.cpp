﻿#include <iostream>

using namespace std;
/*template <typename T>
class Stack
{
public:
	void FillArray(T* BlindArray, T size) 
	{
		for (int i = 0; i < size; i++)
		{
			BlindArray[i] = rand() % 30;
		}
	}

	void ShowArray(T* BlindArray, T size)
	{
		for (T i = 0; i < size; i++)
		{
			std::cout << BlindArray[i] << "\t";
		}
		cout << endl;
	};

	void push_back(T*& BlindArray, T& size, T value)
	{
		int* SecondArray = new int[size + 1];
		for (int i = 0; i < size; i++)
		{
			SecondArray[i] = BlindArray[i];
		}
		SecondArray[size] = value;

		size++;

		delete[] BlindArray;

		BlindArray = SecondArray;
	};
	void pop_back(T*& BlindArray, T& size)
	{
		size--;
		int* SecondArray = new int[size];
		for (int i = 0; i < size; i++)
		{
			SecondArray[i] = BlindArray[i];
		}

		delete[] BlindArray;

		BlindArray = SecondArray;
	};
};

int main()
{
	Stack<int> a;
	
	int size = 10;
	int* FirstArray = new int[size];
	
	a.FillArray(FirstArray, size);
	
	a.ShowArray(FirstArray, size);

	a.pop_back(FirstArray, size);

	a.ShowArray(FirstArray, size);

	a.push_back(FirstArray, size, 111);
	
	a.ShowArray(FirstArray, size);

	delete[] FirstArray;	
}*/



class Animal
{
public:
	Animal() { cout << "Animal constructor" << endl; }
	virtual ~Animal() { cout << "Animal destructor" << endl; }
	virtual void Voice()
	{
		cout << "Woof!" << endl;
	}
	
};

class Dog : public Animal
{
public:
	Dog(){ cout << "Dog constructor" << endl; }
	~Dog() { cout << "Dog destructor" << endl; }

	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal
{
	
public:
	Cat() { cout << "Cat constructor" << endl; }
	~Cat() { cout << "Cat destructor" << endl; }
	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Cow : public Animal
{
public:
	Cow() { cout << "Cow constructor" << endl; }
	~Cow() { cout << "Cow destructor" << endl; }
	
	void Voice() override
	{
		cout << "MUUU!" << endl;
	}
};

int main()
{
	const int size = 3;
	Animal* Animals[size];
	Animals[0] = new Dog();
	Animals[1] = new Cat();
	Animals[2] = new Cow();
	
	for (Animal* a : Animals)
		a->Voice();

	for (int i = 0; i < 3; i++)
	{
		delete[] Animals[i];
	}
}